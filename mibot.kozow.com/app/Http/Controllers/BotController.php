<?php

namespace App\Http\Controllers;
use BotMan\BotMan\BotMan;
use BotMan\BotMan\BotManFactory;
use BotMan\BotMan\Drivers\DriverManager;
use BotMan\Drivers\Telegram\TelegramDriver;
use BotMan\BotMan\Messages\Attachments\File;
use BotMan\BotMan\Messages\Attachments\Audio;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Attachments\Location;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;
use Barryvdh\DomPDF\Facade\Pdf;

class BotController extends Controller
{
public function gestionar()
{
$config = [
// Your driver-specific configuration
"telegram" => [
"token" => env('TELEGRAM_TOKEN')
]
];
DriverManager::loadDriver(TelegramDriver::class);
$botman = BotManFactory::create($config);

$botman->hears('/start', function ($bot) {

// $bot->reply('Bienvenidos/as a mi bot de Telegram con Laravel y Botman.io');
$bot->reply('<b>Bienvenidos/as</b> a mi bot de Telegram con Laravel y Botman.io', ['parse_mode' => 'HTML']);
});





$botman->hears('/labrador', function ($bot) {
// Creamos un adjunto
$attachment = new Image('https://laravel.freeddns.org/labrador.jpg', [
'custom_payload' => true,
]);

// Build message object
$message = OutgoingMessage::create('Foto de un Labrador Retriever')
->withAttachment($attachment);

$bot->reply($message);
});






$botman->hears('/archivos', function ($bot) {
    
    $ch = curl_init(); // Inicio el CURL
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch,CURLOPT_URL,"https://a20davidvg.kozow.com/archivostelegram");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($ch); // Respuesta
    curl_close($ch); // Cierro el CUR

  

    $bot->reply('<b>Archivos en la base de datos refereiciados por el id del cliente:</b>', ['parse_mode' => 'HTML']);


    $infor = str_replace("{ }", " ", $res);
    $message = OutgoingMessage::create($infor);
    $bot->reply($message);
    });  

   

$botman->hears('/clientes', function ($bot) {
    
    $ch = curl_init(); // Inicio el CURL
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch,CURLOPT_URL,"https://a20davidvg.kozow.com/clientestelegram");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($ch); // Respuesta
    curl_close($ch); // Cierro el CUR
    $bot->reply('<b>Clientes en la base de datos:</b>', ['parse_mode' => 'HTML']);
    $message = OutgoingMessage::create($res);
    $bot->reply($message);
    }); 
    


// Con el método sendSticker de la API de Telegram hacemos respuestas a un nivel más bajo.
$botman->hears('/sticker', function ($bot) {
// https://tlgrm.es/stickers

$bot->sendRequest('sendSticker', [
'sticker' => "https://tlgrm.es/_/stickers/4da/ce2/4dace2b4-ecbb-397a-b670-39ad54717826/256/10.webp"
]);
});


$botman->listen();
}
}