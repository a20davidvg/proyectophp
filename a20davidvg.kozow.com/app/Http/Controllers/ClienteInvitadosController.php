<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Cliente;
use Illuminate\Http\Request;
use App\Http\Requests\StoreUser;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Session\Session;
use PDF;

class ClienteInvitadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         // Conseguir todos los eventos
       $clientes = Cliente::all();
       // Para mostrar resultados paginados:
       $clientes = Cliente::paginate(10);    


       return view('clientes.indexInvitados')->with('clientes', $clientes);

    }

    public function clientestelegram()
    {
        //
         // Conseguir todos los eventos
       $clientes = Cliente::all();
       // Para mostrar resultados paginados:
          
       return response()->json($clientes);

    }

    public function clientespdf()
    {
        //
      $clientes2 = Cliente::all();
         // Conseguir todos los eventos
       $clientes = Cliente::all()->toArray();

       
       // Para mostrar resultados paginados:
       $pdf = PDF::loadView('clientes.pdfclientes',["clientes"=>$clientes]);
       
       return $pdf->download("clientes.pdf");;

    }


   
}
