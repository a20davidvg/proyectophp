<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h2>LISTADO DE USUARIOS</h2>
<table class="table">
    <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Usuario</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Email</th>
            <th scope="col">Teléfono</th>
            
        </tr>
    </thead>
    <tbody>
        @foreach($clientes as $cliente)
        <tr>
            <th scope="row">{{ $cliente["id"] }}</th>
            <th scope="row">{{ $cliente["usuario"] }}</th>
            <th scope="row">{{ $cliente["nombre"]}}</th>
            <th scope="row">{{ $cliente["apellidos"] }}</th>
            <th scope="row">{{ $cliente["email"] }}</th>
            <th scope="row">{{ $cliente["telefono"] }}</th>
            
        </tr>
        @endforeach
    </tbody>
</table>

</body>
</html>